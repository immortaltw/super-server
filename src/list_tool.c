#include <list_tool.h>

client_list *client_list_create() {
	client_list *ret = malloc(sizeof(client_list));
	if (ret) {
		ret->count = 0;
		ret->first = NULL;
		ret->last = NULL;
	}
	return ret;
}

void client_list_destroy(client_list *list) {
	LIST_FOREACH(list, first, next, cur) {
		if (cur->prev) {
			free(cur->prev);	
		}
	}
	free(list->last);
	free(list);
}

void client_list_push(client_list *list, int clnt_sock) {
	client *node = calloc(1, sizeof(client));
	check_mem(node);	

	node->sock = clnt_sock;
	if (list->last == NULL) {
		list->first = node;
		list->last = node;
	} else {
		list->last->next = node;
		node->prev = list->last;
		list->last = node;
	}
	list->count++;
error:
	return;
}
