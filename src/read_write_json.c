#include <read_write_json.h>

void write_to_saved_state(cJSON *in) {

	char *out;
	FILE *state_file = fopen(STATE_FILE_NAME, "w+");
	check_mem(state_file);
	out = cJSON_Print(in);
	fprintf(state_file, out);
	
	free(out);
	fclose(state_file);

error:
	return;
}

cJSON *read_from_saved_state() {
	cJSON *ret;
	char *out;
	FILE *state_file = fopen(STATE_FILE_NAME, "r+");
	check_mem(state_file);
	
	/* Get file length. */
	fseek(state_file, 0, SEEK_END);
	long int len = ftell(state_file);
	fseek(state_file, 0, SEEK_SET);

	out = (char *)malloc(len+1);	
	check_mem(out);

	fread(out, 1, len, state_file);
	ret = cJSON_Parse(out);
	free(out);
	return ret;

error:
	return NULL;
}

char *get_sender(cJSON *in) {
	if (!in) return NULL;
	char *sender = cJSON_GetObjectItem(in, "sender")->valuestring;
	return sender;
}

char *get_state(cJSON *in) {
	if (!in) return NULL;
	char *state = cJSON_GetObjectItem(in, "state")->valuestring; 
	return state;
}

char *get_img(cJSON *in) {
	if (!in) return NULL;
	char *img = cJSON_GetObjectItem(in, "img")->valuestring; 
	return img;
}
