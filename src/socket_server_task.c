#include <socket_server_task.h>

/**
 * Release resources taken.
 */
void deinit_server() {
	cJSON *json = cJSON_CreateObject();
	cJSON_AddStringToObject(json, "sender", "server");
	cJSON_AddStringToObject(json, "state", "break");
	cJSON_AddStringToObject(json, "img", global_server_data.current_img);
	write_to_saved_state(json);
	cJSON_Delete(json);

	client_list_destroy(global_server_data.clients);
	pthread_mutex_destroy(&global_server_data.server_lock);
	pthread_cond_destroy(&global_server_data.server_cv);
		
	closelog();
}

/**
 * Handlers for these two signals, in case the server is terminated
 * by unexpected reasons.
 */
void sig_handler(int signo) {
	switch (signo) {
		case SIGINT:
		case SIGSEGV:
			syslog(LOG_INFO, "####### Server terminated #######");
			printf("####### Server terminated #######\n");
			deinit_server();
			exit(0);
		default:
			break;
	}
}

/** 
 * TODO: Use a better way to implement FSM.
 * This is a FSM that has 4 states:
 * init; send_update; recv_update; idle.
 */
char *handle_init_state(int clnt_sock) {
	pthread_mutex_lock (&global_server_data.server_lock);
	client_list_push(global_server_data.clients, clnt_sock);
	pthread_mutex_unlock (&global_server_data.server_lock);
	return "send_update";
}

char *handle_recv_update_state(int clnt_sock) {
	/* Notify all the threads that are in idle state. */
	pthread_mutex_lock (&global_server_data.server_lock);
	pthread_cond_broadcast(&global_server_data.server_cv);
	pthread_mutex_unlock (&global_server_data.server_lock);

	cJSON *json = cJSON_CreateObject();
	char *out;

	cJSON_AddStringToObject(json, "sender", "server");
	cJSON_AddStringToObject(json, "state", "idle");
	cJSON_AddStringToObject(json, "img", "");

	out=cJSON_Print(json);
	send(clnt_sock, out, strlen(out), 0);

	cJSON_Delete(json);
	free(out);

	return "idle";
}

char *handle_send_update_state(int clnt_sock) {
	pthread_mutex_lock (&global_server_data.server_lock);

	cJSON *json = cJSON_CreateObject();
	char *out;

	cJSON_AddStringToObject(json, "sender", "server");
	cJSON_AddStringToObject(json, "state", "send_update");
	cJSON_AddStringToObject(json, "img", global_server_data.current_img);

	out=cJSON_Print(json);
	send(clnt_sock, out, strlen(out), 0);

	pthread_mutex_unlock (&global_server_data.server_lock);
	
	cJSON_Delete(json);
	free(out);
	return "idle";
}

char *handle_idle_state() {
	pthread_mutex_lock (&global_server_data.server_lock);
	pthread_cond_wait(&global_server_data.server_cv, &global_server_data.server_lock);
	pthread_mutex_unlock (&global_server_data.server_lock);
	return "send_update";
}

void handle_client_connection(int clnt_sock, cJSON *json) {
	char *local_state = get_state(json);
	while (true) {
		if (!strcmp(local_state, "init")) {
			local_state = handle_init_state(clnt_sock);	
		} else if (!strcmp(local_state, "recv_update")) {
			local_state = handle_recv_update_state(clnt_sock);
		} else if (!strcmp(local_state, "send_update")) {
			local_state = handle_send_update_state(clnt_sock);
		} else if (!strcmp(local_state, "idle")) {
			local_state = handle_idle_state();
		} else if (!strcmp(local_state, "break")){
			return;	
		}
	}
}

/**
 * Receive command and data sent by client.  
 */
void handle_incoming_connection(int clnt_sock) {
	int recv_msg_size = 0;
	int total_size = 0;
	
	while ((recv_msg_size=recv(clnt_sock, serv_buf+total_size, SERV_BUF_SIZE, 0)) > 0) {
		printf("Receving %s\n", serv_buf);
		check_api(recv_msg_size, "Receive client data failed.");
		total_size += recv_msg_size;
		if (total_size >= SERV_BUF_SIZE) {
			syslog(LOG_ERR, "Not enough buffer size.");
			errno = 0;
			goto error;
		}
	}

	cJSON *json = cJSON_Parse(serv_buf);
	handle_client_connection(clnt_sock, json);
	send(clnt_sock, "Ack", 3, 0);

error:
	close(clnt_sock);
	cJSON_Delete(json);
	return; 
}

/**
 * Initialize server configuration. Hardcoded.
 */
void init_server_config() {
	/* TODO: Read it from the config file */
	openlog("serverlog", LOG_PID|LOG_CONS, LOG_USER);

	/* No need to synchronize at this stage */
	global_server_data.clients = client_list_create();
	global_server_data.current_img = get_img(read_from_saved_state());
	pthread_mutex_init(&global_server_data.server_lock, NULL);
	pthread_cond_init (&global_server_data.server_cv, NULL);

	/* Register signal handlers. */

	struct sigaction sa;
    sa.sa_handler = &sig_handler;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) == -1) {
		syslog(LOG_ERR, "Can not handle SIGINT");
	}

    if (sigaction(SIGSEGV, &sa, NULL) == -1) {
		syslog(LOG_ERR, "Can not handle SIGINT");
	}
}

/**
 * Thread function to handle incoming connection.
 */
void *server_thread_task(void *args) {
	check_mem(args);
	pthread_detach(pthread_self());
	int clnt_sock = ((thread_args *)args)->clnt_sock;
	free(args);
	handle_incoming_connection(clnt_sock);

error:
	pthread_exit(NULL);
}

/**
 * Socket server.
 */
int create_server_socket(unsigned short port) {
	int ret = -1;
	int server_sock;
	struct sockaddr_in addr;

	/* Create a socket for the server. */
	ret = server_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);	
	check_api(ret, "Server socket create failed.");

	/* Fill in the addr struct. */
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);

	/* Bind */
	ret = bind(server_sock, (struct sockaddr *)&addr, sizeof(addr));
	check_api(ret, "Server socket bind failed.");	

	/* From this point on this socket listens to incoming connection. */
	ret = listen(server_sock, MAXCONN);
	check_api(ret, "Server socket listen failed.");
	log_info("Listening to incoming connection.");

	return server_sock;

error:
	return ret;
}

/**
 * As its name suggests. It accepts the incoming connection
 * and returns client socket fd.
 */
int accept_incoming_connection(int server_sock) {
	int ret = -1;
	int clnt_sock;
	struct sockaddr_in clnt_addr;
	unsigned int clnt_len = sizeof(clnt_sock);

	log_info("Before accepted incoming connection.");
	ret = clnt_sock = accept(server_sock, (struct sockaddr *)&clnt_addr, &clnt_len);
	check_api(ret, "Server socket accept failed.");
	log_info("accepted incoming connection.");

	return clnt_sock;

error:
	return ret;
}

/**
 * Where everything begins.....
 */
void start_server() {
	int server_sock;
	int clnt_sock;
	int ret = -1;
	thread_args *args;
	pthread_t thread_id;

	init_server_config();

	server_sock = create_server_socket(SERV_PORT);
	syslog(LOG_INFO, "Server with port # %d created", SERV_PORT);
	
	while(true) {
		clnt_sock = accept_incoming_connection(server_sock);
		args = (thread_args*)malloc(sizeof(thread_args));	
		check_mem(args);
		args->clnt_sock = clnt_sock;

		ret = pthread_create(&thread_id, NULL, server_thread_task, (void *)args);
		check_api(ret, "Thread creation failed.");
		printf("thread %ld created\n", (long int)thread_id);
	}

error:
	deinit_server();
	return;
}
