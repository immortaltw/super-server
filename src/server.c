#include <stdio.h>
#include <stdlib.h>
#include <socket_server_task.h>

int main(void) {
	log_info("Server launched");
	start_server();
	exit(0);
}
