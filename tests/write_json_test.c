#include <common.h>
#include <cJSON.h>
#include <read_write_json.h>
#include "minunit.h"

static cJSON *json = NULL;
char *sender = "client";
char *state = "init";
char *img; 

char *test_create() {
	FILE *bmp = fopen("red.bmp", "rb");
	mu_assert(bmp != NULL, "Failed to create bmp.");
	fseek(bmp, 0, SEEK_END);
	long int len = ftell(bmp);
	fseek(bmp, 0, SEEK_SET);
	char *data = (char *)malloc(len+1);
	fread(data, 1, len, bmp);
	img = data;

	/* cJSON kicks in. */
	json = cJSON_CreateObject();
	mu_assert(json != NULL, "Failed to create json obj.");
	fseek(bmp, 0, SEEK_END);
	cJSON_AddStringToObject(json, "sender", sender);
	cJSON_AddStringToObject(json, "state", state);
	cJSON_AddStringToObject(json, "img", data);

	return NULL;
}

char *test_destroy() {
	cJSON_Delete(json);
	free(img);
	return NULL;
}

char *test_read_write_saved_state() {
	cJSON *res;
	write_to_saved_state(json);
	res = read_from_saved_state();
	char *img_from_json = cJSON_GetObjectItem(json, "img")->valuestring;
	char *img_from_file = cJSON_GetObjectItem(res, "img")->valuestring; 

	mu_assert(strcmp(img_from_json, img_from_file) == 0, "read write saved state test failed.");
	cJSON_Delete(res);
	
	return NULL;
}

char *test_get_sender() {
	char *sender_from_file = get_sender(json);
	mu_assert(strcmp(sender, sender_from_file) == 0, "get sender test failed.");

	return NULL;
}

char *test_get_state() {
	char *state_from_file = get_state(json);
	mu_assert(strcmp(state, state_from_file) == 0, "get state test failed.");

	return NULL;
}

char *test_get_img() {
	char *img_from_file = get_img(json);
	mu_assert(strcmp(img, img_from_file) == 0, "get img test failed.");

	return NULL;
}

char *all_tests() {
    mu_suite_start();

    mu_run_test(test_create);
    mu_run_test(test_read_write_saved_state);
    mu_run_test(test_get_sender);
    mu_run_test(test_get_img);
    mu_run_test(test_destroy);

    return NULL;
}

RUN_TESTS(all_tests);

