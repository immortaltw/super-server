# This makefile refers to the template in 
# http://c.learncodethehardway.org/book/ex28.html
# (Sorry I don't know how to include proper licensing info)

UNAME := $(shell uname)
IDIR=./include
CFLAGS=-g -O2 -Wall -Wextra -I$(IDIR) -DNDEBUG $(OPTFLAGS)
LIBS=-lpthread

SERVER_SRC=src/server.c
SERVER_OBJ=$(patsubst %c,%o,$(SERVER_SRC))

SOURCES=$(filter-out $(SERVER_SRC), $(wildcard src/**/*.c src/*.c))
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

TEST_SRC=$(wildcard ./tests/*.c)
TESTS=$(patsubst %.c,%,$(TEST_SRC))

TARGET=build/libserver.a
PROGRAM=bin/server

# Target Build
all: $(PROGRAM) $(TARGET) tests

dev: CFLAGS=-g -Wall -Isrc -Wall -Wextra $(OPTFLAGS)
dev: all

$(PROGRAM): build $(OBJECTS) $(SERVER_OBJ)
	$(CC) -o $@ $(LIBS) $(OBJECTS) $(SERVER_OBJ)

$(TARGET): CFLAGS += -fPIC
$(TARGET): build $(OBJECTS)
	ar rcs $@ $(OBJECTS)
	ranlib $@

build:
	@mkdir -p build
	@mkdir -p bin

# Unit Tests
.PHONY: tests
tests: CFLAGS += $(TARGET)
tests: $(TESTS)
	sh ./tests/runtests.sh

# Cleaner
clean:
	rm -rf build $(OBJECTS) $(TESTS) $(SERVER_OBJ)
	rm -f tests/tests.log
	find . -name "*.gc*" -exec rm {} \;
	rm -rf `find . -name "*.dSYM" -print`

