## 0. Overview

A server application running in the background that receives commands from
clients and sends update command to clients. The server is implemented in C.

In the client's point of view, the server helps it to know which image to
display. If itself or it's peer sends an update command with a new image to
server, the server sends this update to all the other clients except the sender
with the new image. After this op, all the clients UI should display the new image.

___

## 1. The Server

The server can do the following:

1. Allow multiple client connections.

2. Keep one bitmap of current canvas status.

3. Log events to syslog.

4. Save the state upon termination (normal and abnormal) with a
configurable filename.

The following section will discuss briefly about how each
of the functions mentioned above is implemented.

### 1.1 Multiple Client Connections

This will be implemented with socket. TCP is used because we do not have any
error correction mechanism in the application layer. A naive
one-thread-per-client-connection mechanism will be utilized with a configurable
maximum cocurrent connection limit. 

### 1.2 Single Canvas Status.

The image format being transferred among the GUI and clients is transparent to
the server.

### 1.3 Log to Syslog.

Use syslog module.

### 1.4 Save the State Upon Termination.

Signals and exits codes will be used here.

### Test Cases

Not fully completed yet.

## Future Work

Couple of possible improvements are briefly discussed in this section.

- Server should have a thread pool to minimize the overhead introduced by
  creating one thread per client connection.

- Json format is utilized as a message exchanging format. In the future it can be
  replaced by google protocol buffer.

