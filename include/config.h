#ifndef _H_CONFIG
#define _H_CONFIG

/* The maximum allowed outstanding connection. */
#define MAXCONN 10

/* Default server port. */
#define SERV_PORT 12500

/* Maximum receive buffer size. */
#define SERV_BUF_SIZE 512

/* The saved state file name. */
#define STATE_FILE_NAME "./tests/state.json"

#endif
