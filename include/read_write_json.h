#ifndef _H_READ_WRITE_JSON
#define _H_READ_WRITE_JSON

#include <cJSON.h>
#include <common.h>

/**
 * @brief The saved state json file contains the last bmp.
 *
 * TODO: Use google protocol buffer.
 * The json format looks like this:
 *
 * {
 * 		sender: "client"|"server",
 *		state: "init"|"update",
 * 		img: <bmp stored in char string>;
 * }
 * 
 * File name is stored in config.h
**/
void write_to_saved_state(cJSON *in);

/**
 * @brief Return the saved state file in char string.
**/
cJSON *read_from_saved_state();

/**
 * @brief To figure out who the sender is.
 *
 * The json format transferred between server/client and 
 * server/GUI looks like this:
 *
 * {
 * 		sender: "client"|"server",
 *		state: "init"|"update",
 *		img: <bmp stored in char string>;
 * }
 *
 * Return the sender name.
**/
char *get_sender(cJSON *in);

/**
 * @brief Return the state in char string.
**/
char *get_state(cJSON *in);

/**
 * @brief Return the image file in char string.
**/
char *get_img(cJSON *in);

#endif
