#ifndef _H_SOCKET_SERVER_TASK
#define _H_SOCKET_SERVER_TASK

#include <common.h>
#include <list_tool.h>
#include <pthread.h>
#include <signal.h>
#include <read_write_json.h>

char serv_buf[SERV_BUF_SIZE];

/**
 * @brief Global struct that holds server data.
 *
**/
typedef struct _server_data {
	char *current_img;
	client_list *clients;
	pthread_mutex_t server_lock;
	pthread_cond_t server_cv;
} server_data;

typedef struct _thread_args{
	int clnt_sock;
} thread_args;

static server_data global_server_data;

/**
 * @brief Signal handler.
**/

void sig_handler(int signo);

/**
 * @brief Event handlers for incoming connection.
**/
char *handle_init_state(int clnt_sock);
char *handle_recv_update_state(int clnt_sock); 
char *handle_send_update_state(int clnt_sock); 
char *handle_idle_state();

void handle_client_connection(int clnt_sock, cJSON *json);
void handle_incoming_connection(int clnt_sock);

/**
 * @brief Server related utilities.
 *
**/
void deinit_server();
void init_server_config();
void *server_thread_task(void *args);
int create_server_socket(unsigned short server_port);
int accept_incoming_connection(int server_sock);
void start_server();

#endif
