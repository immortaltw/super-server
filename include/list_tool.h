#ifndef _H_LIST_TOOL 
#define _H_LIST_TOOL

#include <common.h>

/* This code refers to http://c.learncodethehardway.org/book/ex32.html */

typedef struct _client {
	struct _client *next;
	struct _client *prev;
	int sock;
} client;

typedef struct _clients {
	int count;
	client *first;
	client *last;
} client_list;

client_list *client_list_create();
void client_list_destroy(client_list *list);
void client_list_push(client_list *list, int clnt_sock);

#define LIST_FOREACH(L, S, M, V) client *_node = NULL; \
	client *V = NULL; \
	for (V = _node = L->S; _node != NULL; V = _node = _node->M)

#endif
